#include <iostream>
#include <memory>
#include <string.h>
#include "ConservativeBusTours.hpp"

using namespace std;

string HELP = "The program for calculating the tourist index. To simplify the input, you can use the input data, which is located in the datapub folder, where .in is the input data and .out is the result you should get.\n"
              "Description:\n"
              "-thread - used to specify the number of threads, after this description the number of threads should be written (for example -thread 4)\n"
              "-repeat - used to specify the number of repetitions of the program, after this description the number of repetitions should be written (for example -repeat 2)\n"
              "-test - used when we have multiple threads enabled. After executing a multi-threaded program, runs the program with only one thread so that the result can be compared.\n"
              "Example using all descriptions:\n"
              "-repeat 2 -thread 4 -test ";

void control(int argc, char *argv[], bool &isHelp, bool &test, int &threadCount, int &repeatNumber, string &error) {
    for (int i = 1; i < argc; ++i) {
        if (strcmp(argv[i], "--help") == 0) {
            isHelp = true;
            break;
        } else if (strcmp(argv[i], "-thread") == 0) {
            i++;
            if (i >=argc ) {
                error = "after the description \"-thread\" there must be a number of threads";
                break;
            } else {
                try {
                    threadCount = std::stoi(argv[i]);
                } catch (...) {
                    error = "after the description \"-thread\" there must be a number(only integer type) of threads";
                    break;
                }
            }
        } else if (strcmp(argv[i], "-repeat") == 0) {
            i++;
            if (i >=argc ) {
                error = "after the description \"-repeat\" there must be a number of repetitions";
                break;
            } else {
                try {
                    repeatNumber = std::stoi(argv[i]);
                    if (repeatNumber <= 0) {
                        error = "after the description \"-repeat\" there must be a number(only integer type) of repetitions";
                        break;
                    }
                } catch (...) {
                    error = "after the description \"-repeat\" there must be a number(only integer type) of repetitions";
                    break;
                }
            }
        } else if (strcmp(argv[i], "-test") == 0) {
            test = true;
            if ( threadCount <= 1 ) {
                error = "The number of threads must be more than 1 and they must be specified before the \"-test\" description";
                break;
            }
        } else {
            error = "Unknown description – \"" + string(argv[i]) + "\"";
            break;
        }
    }
}

template <typename TimePoint>
std::chrono::milliseconds to_ms(TimePoint tp) {
    return std::chrono::duration_cast<std::chrono::milliseconds>(tp);
}

int main(int argc, char *argv[]){
    auto isHelp = false;
    auto test = false;
    string error;
    int threadCount = 1;
    int repeatNumber = 1;
    control(argc, argv, isHelp, test, threadCount, repeatNumber, error);
    if (!error.empty()) {
        cout << "\033[1;31m" << error << "\033[0m\n" << endl;
    } else if (isHelp) {
        cout << HELP << endl;
    } else {
        int number_of_village, start_village, end_village, max_visit_time;
        cout << "Enter the number of villages, starting village, finishing village, maximum allowed excursion time:" << endl;
        cin >> number_of_village >> start_village >> end_village >> max_visit_time;
        ConservativeBusTours bus_tur(number_of_village, start_village, end_village, max_visit_time);
        cout << "Enter tourist indexes of villages in order:" << endl;
        for (int i = 0, idx; i < number_of_village && cin >> idx; i++) {
            bus_tur.addTouristIndex(idx);
        }
        cout << "Enter the visit time of each village in order:" << endl;
        for (int i = 0, visit; i < number_of_village && cin >> visit; i++) {
            bus_tur.addVisitTime(visit);
        }
        cout << "Enter in all lines three numbers, the first two specify the labels of the villages and the third value specifies the ride time of the road between these two villages:" << endl;
        for (int i = 0; i < number_of_village; i++) {
            int from = 0, to = 0, time;
            cin >> from >> to >> time;
            bus_tur.addRoad(from, to, time);
        }

        cout << "Start calculate" << endl;
        auto start = std::chrono::high_resolution_clock::now();
        auto end = std::chrono::high_resolution_clock::now();

        bus_tur.searchBackboneVillages();

        bus_tur.searchConservativeBusTours();

        for (int i = 0; i < repeatNumber; ++i) {
            start = std::chrono::high_resolution_clock::now();
            cout << bus_tur.knapsack(threadCount) << endl;
            end = std::chrono::high_resolution_clock::now();
            std::cout << "Needed " << to_ms(end - start).count() << " ms to finish.\n";
            if (repeatNumber != 1 && i != repeatNumber - 1) {
                std::cout << "________________________________________________" << std::endl;
            }
        }
        if (test) {
            std::cout << "____________________Test________________________" << std::endl;
            for (int i = 0; i < repeatNumber; ++i) {
                start = std::chrono::high_resolution_clock::now();
                cout << bus_tur.knapsack() << endl;
                end = std::chrono::high_resolution_clock::now();
                std::cout << "Needed " << to_ms(end - start).count() << " ms to finish.\n";
                if (repeatNumber != 1 && i != repeatNumber - 1) {
                    std::cout << "________________________________________________" << std::endl;
                }
            }
        }
    }
    return 0;
}