#include <iostream>
#include <memory>
#include <vector>
#include <map>
#include <queue>
#include <thread>
#include <mutex>
#include <tuple>
#include "ConservativeBusTours.hpp"

using namespace std;

ConservativeBusTours::ConservativeBusTours(int n, int a, int b, int t) {
    number_of_village = n;
    start_village = a;
    end_village = b;
    max_visit_time = t;
    map = vector<Village>(number_of_village);
}

void ConservativeBusTours::addTouristIndex(int tourist_index) {
    map[counter_turist_index++].tourist_index = tourist_index;
}

void ConservativeBusTours::addVisitTime(int visit_time) {
    map[counter_visit_time++].visit_time = visit_time;
}

void ConservativeBusTours::addRoad(int from, int to, int time) {
    map[from].road[to] = time;
    map[to].road[from] = time;
}

int ConservativeBusTours::knapsack(int maxThreadNumber) {
    int conservative_time = max_visit_time - sum_main_time + 1;
    int not_backbone_size = (int)not_backbone.size() + 1;
    vector<vector<int>> m(not_backbone_size, vector<int>(conservative_time, 0));

    std::vector<std::thread> threads;
    std::vector<mutex> locks(maxThreadNumber);
    vector<vector<bool>> m_for_threads(not_backbone_size, vector<bool>(conservative_time, false));

    for (int i = 0; i < maxThreadNumber; ++i) {
        locks[i].lock();
    }

    for (int i = 0; i < conservative_time; ++i) {
        m_for_threads[0][i] = true;
    }

    auto tmp_func = [&](int threadNumber, int step) {
        for (int i = 1 + threadNumber; i < not_backbone_size; i += step) {
            for (int j = 0; j < conservative_time; ++j) {
                //control if the previous thread has already counted the number at the top
                if (!m_for_threads[i - 1][j]) {
                    locks[threadNumber].lock();
                }
                int time = map[not_backbone[i - 1]].visit_time + \
                    2 * map[not_backbone[i - 1]].road.begin()->second;
                int tmp = m[i - 1][j];
                if (time > j) {
                    m[i][j] = tmp;
                } else {
                    if (m[i - 1][j] > m[i - 1][j - time] + map[not_backbone[i - 1]].tourist_index) {
                        m[i][j] = tmp;
                    } else {
                        m[i][j] = m[i - 1][j - time] + map[not_backbone[i - 1]].tourist_index;
                    }
                }
                //shows that the number is counted here
                m_for_threads[i][j] = true;
                //unlock next thread
                if (threadNumber + 1 < maxThreadNumber) {
                    locks[threadNumber + 1].unlock();
                } else {
                    locks[0].unlock();
                }
            }
        }
    };

    threads.reserve(maxThreadNumber);
    int step = maxThreadNumber;
    for (int i = 0; i < maxThreadNumber; ++i) {
        threads.emplace_back(std::thread(tmp_func, i, step));
    }

    for (int i = 0; i < maxThreadNumber; ++i) {
        threads[i].join();
    }
    return m[not_backbone_size - 1][conservative_time - 1] + sum_main_index;
}

int ConservativeBusTours::knapsack() {
    int conservative_time = max_visit_time - sum_main_time;
    int not_backbone_size =  not_backbone.size();
    vector<vector<int>> m(not_backbone_size + 1, vector<int>(conservative_time+1, 0));
    for (int i = 1; i < not_backbone_size + 1; ++i) {
        for (int j = 0; j < conservative_time + 1; ++j) {
            int time = map[not_backbone[i - 1]].visit_time +\
                2 * map[not_backbone[i - 1]].road.begin()->second;
            if(time > j){
                m[i][j] = m[i-1][j];
            }else{
                if(m[i-1][j] > m[i-1][j-time] + map[not_backbone[i - 1]].tourist_index){
                    m[i][j] = m[i-1][j];
                }else{
                    m[i][j] = m[i-1][j-time] + map[not_backbone[i - 1]].tourist_index;
                }
            }
        }
    }
    return m[not_backbone_size][conservative_time] + sum_main_index;
}

void ConservativeBusTours::searchBackboneVillages() {
    queue<int> q;
    vector<int> parents(number_of_village, -2);
    vector<int> destination(number_of_village, -1);
    vector<bool> visited(number_of_village, false);
    q.push(start_village);
    parents[start_village] = -1;
    destination[start_village] = 0;
    while (!q.empty() && !visited[end_village]){
        int cur = q.front();
        q.pop();
        visited[cur] = true;
        for(auto i = map[cur].road.begin(); i != map[cur].road.end(); i++){
            if(!visited[i->first] && !visited[end_village]){
                destination[i->first] = destination[cur] + 1;
                parents[i->first] = cur;
                q.push(i->first);
            }
            if (i->first == end_village) {
                visited[end_village] = true;
            }
        }
    }
    backbone = vector<int>(destination[end_village] + 1);
    backbone[0]=end_village;
    map[end_village].main_road = true;
    for (int i = end_village, j=1; parents[i]!=-1; i=parents[i]) {
        backbone[j] = parents[i];
        map[backbone[j++]].main_road = true;
    }
}

void ConservativeBusTours::searchConservativeBusTours() {
    for(unsigned long i = 0; i < backbone.size(); i++){
        sum_main_index += map[backbone[i]].tourist_index;
        // time for visit
        sum_main_time += map[backbone[i]].visit_time;
        if (i+1 < backbone.size()) {
            // time for road
            sum_main_time += map[backbone[i]].road.find(backbone[i+1])->second;
        }
        for (auto j = map[backbone[i]].road.begin(); j != map[backbone[i]].road.end(); j++) {
            if (!map[j->first].main_road) {
                not_backbone.push_back(j->first);
                map[j->first].road.clear();
                map[j->first].road[backbone[i]] = j->second;
            }
        }
    }
}



//#include <iostream>
//#include <memory>
//#include <vector>
//#include <map>
//#include <queue>
//#include <thread>
//#include <mutex>
//
//using namespace std;
//
//class ConservativeBusTours{
//private:
//    struct Village{
//        int visit_time;
//        int tourist_index;
//        bool main_road = false;
//        map<int, int> road;
//    };
//    int number_of_village;
//    int start_village;
//    int end_village;
//    int max_visit_time;
//    int counter_turist_index = 0;
//    int counter_visit_time = 0;
//    int sum_main_index = 0;
//    int sum_main_time = 0;
//    vector<Village> map;
//    vector<int> backbone;
//    vector<int> not_backbone;
//public:
//    ConservativeBusTours(int n, int a, int b, int t){
//        number_of_village = n;
//        start_village = a;
//        end_village = b;
//        max_visit_time = t;
//        map = vector<Village>(number_of_village);
//    }
//
//    void addTouristIndex(int tourist_index){
//        map[counter_turist_index++].tourist_index = tourist_index;
//    }
//
//    void addVisitTime(int visit_time){
//        map[counter_visit_time++].visit_time = visit_time;
//    }
//
//    void addRoad(int from, int to, int time){
//        map[from].road[to] = time;
//        map[to].road[from] = time;
//    }
//
//    int knapsack(int maxThreadNumber){
//        int conservative_time = max_visit_time - sum_main_time + 1;
//        int not_backbone_size = (int)not_backbone.size() + 1;
//        vector<vector<int>> m(not_backbone_size, vector<int>(conservative_time, 0));
//
//        std::vector<std::thread> threads;
//        std::vector<mutex> locks(maxThreadNumber);
//        vector<vector<bool>> m_for_threads(not_backbone_size, vector<bool>(conservative_time, false));
//
//        for (int i = 1; i < maxThreadNumber; ++i){
//            locks[i].lock();
//        }
//
//        for (int i = 0; i < conservative_time; ++i) {
//            m_for_threads[0][i] = true;
//        }
//
//        auto tmp_func = [&](int threadNumber, int step) {
//            for (int i = 1 + threadNumber; i < not_backbone_size; i += step) {
//                for (int j = 0; j < conservative_time; ++j) {
//                    while (!m_for_threads[i - 1][j]) {
//                        locks[threadNumber].lock();
//                    }
//                    int time = map[not_backbone[i - 1]].visit_time + \
//                        2 * map[not_backbone[i - 1]].road.begin()->second;
//                    int tmp = m[i - 1][j];
//                    if (time > j) {
//                        m[i][j] = tmp;
//                    } else {
//                        if (m[i - 1][j] > m[i - 1][j - time] + map[not_backbone[i - 1]].tourist_index) {
//                            m[i][j] = tmp;
//                        } else {
//                            m[i][j] = m[i - 1][j - time] + map[not_backbone[i - 1]].tourist_index;
//                        }
//                    }
//                    m_for_threads[i][j] = true;
//                    locks[threadNumber].unlock();
//                    if (threadNumber + 1 < maxThreadNumber) {
//                        locks[threadNumber + 1].unlock();
//                    } else {
//                        locks[0].unlock();
//                    }
//                }
//            }
//        };
//
//        threads.reserve(maxThreadNumber);
//        int step = maxThreadNumber;
////        cout << "step = " << step << endl;
//        for (int i = 0; i < maxThreadNumber; ++i){
//            threads.emplace_back(std::thread(tmp_func, i, step));
//        }
//
//        for (int i = 0; i < maxThreadNumber; ++i){
//            threads[i].join();
//        }
//        return m[not_backbone_size - 1][conservative_time - 1] + sum_main_index;
//    }
//
//    void searchBackboneVillages(){
//        queue<int> q;
//        vector<int> parents(number_of_village, -2);
//        vector<int> destination(number_of_village, -1);
//        vector<bool> visited(number_of_village, false);
//        q.push(start_village);
//        parents[start_village] = -1;
//        destination[start_village] = 0;
//        while (!q.empty() && !visited[end_village]){
//            int cur = q.front();
//            q.pop();
//            visited[cur] = true;
//            for(auto i = map[cur].road.begin(); i != map[cur].road.end(); i++){
//                if(!visited[i->first] && !visited[end_village]){
//                    destination[i->first] = destination[cur] + 1;
//                    parents[i->first] = cur;
//                    q.push(i->first);
//                }
//                if (i->first == end_village) {
//                    visited[end_village] = true;
//                }
//            }
//        }
//        backbone = vector<int>(destination[end_village] + 1);
//        backbone[0]=end_village;
//        map[end_village].main_road = true;
//        for (int i = end_village, j=1; parents[i]!=-1; i=parents[i]) {
//            backbone[j] = parents[i];
//            map[backbone[j++]].main_road = true;
//        }
//    }
//
//    void searchConservativeBusTours(){
//        for(unsigned long i = 0; i < backbone.size(); i++){
//            if(!map[backbone[i]].main_road){
//                cout << "false" << endl;
//            }
//        }
//        for(unsigned long i = 0; i < backbone.size(); i++){
//            sum_main_index += map[backbone[i]].tourist_index;
//            sum_main_time += map[backbone[i]].visit_time;
//            if (i+1 < backbone.size()) {
//                sum_main_time += map[backbone[i]].road.find(backbone[i+1])->second;
//            }
//            for (auto j = map[backbone[i]].road.begin(); j != map[backbone[i]].road.end(); j++) {
//                if (!map[j->first].main_road){
//                    not_backbone.push_back(j->first);
//                    map[j->first].road.clear();
//                    map[j->first].road[backbone[i]] = j->second;
//                }
//            }
//        }
//    }
//};
