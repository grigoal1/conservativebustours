#include <iostream>
#include <memory>
#include <vector>
#include <map>
#include <queue>
#include <thread>
#include <mutex>


class ConservativeBusTours{
private:
    struct Village{
        int visit_time;
        int tourist_index;
        bool main_road = false;
        std::map<int, int> road;
    };
    int number_of_village;
    int start_village;
    int end_village;
    int max_visit_time;
    int counter_turist_index = 0;
    int counter_visit_time = 0;
    int sum_main_index = 0;
    int sum_main_time = 0;
    std::vector<Village> map;
    std::vector<int> backbone;
    std::vector<int> not_backbone;
public:
    ConservativeBusTours(int n, int a, int b, int t);

    void addTouristIndex(int tourist_index);

    void addVisitTime(int visit_time);

    void addRoad(int from, int to, int time);

    /**
     * Calculate the best travel index
     * @param maxThreadNumber – indicates how many threads to use
     * @return the maximal possible tour index
     */
    int knapsack(int maxThreadNumber);

    /**
     * Calculate the best travel index(implementation for one thread)
     * @return the maximal possible tour index
     */
    int knapsack();



    /**
     * search main road(backbone)
     */
    void searchBackboneVillages();

    void searchConservativeBusTours();
};